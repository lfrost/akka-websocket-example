/*
 * Copyright © 2017-2022 Lyle Frost <lfrost@cnz.com>
 */

package controllers

import javax.inject.Inject

import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents}

/** The main controller.
 *
 *  @param assetsFinder Asset finder
 *  @param components   The base controller components
 */
class ApplicationController @Inject()(
    val components : ControllerComponents
)(
    implicit val assetsFinder : AssetsFinder
) extends AbstractController(components) {
    implicit private val defaultMarkerContext = play.api.MarkerContext.NoMarker
    private          val logger               = play.api.Logger(this.getClass)

    /** The main route.
     *
     *  @param path The path portion of the request URI
     *  @return     HTTP response
     */
    def index(path:String) : Action[AnyContent] = Action { implicit request =>
        logger.debug(s"${request.method} index(path = ${path})")
        Ok(views.html.index(path))
    }
}
