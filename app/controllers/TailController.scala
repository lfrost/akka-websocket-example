/*
 * Copyright © 2017-2022 Lyle Frost <lfrost@cnz.com>
 */

package controllers

import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets.UTF_8
import java.nio.file.{Files, FileSystems, Path}
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Left, Right, Success, Try}

import akka.NotUsed
import akka.stream.alpakka.file.scaladsl.FileTailSource
import akka.stream.scaladsl.{Flow, Sink, Source}
import play.api.Configuration
import play.api.mvc.{AbstractController, Action, AnyContent, ControllerComponents, Request, WebSocket}
import play.utils.UriEncoding.encodePathSegment

/** The Tail controller.
 *
 *  @param assetsFinder  asset finder
 *  @param configuration application configuration
 *  @param components    the base controller components
 */
class TailController @Inject()(
    val components    : ControllerComponents,
    val configuration : Configuration
)(
    implicit val assetsFinder : AssetsFinder
) extends AbstractController(components) {
    implicit private val defaultMarkerContext = play.api.MarkerContext.NoMarker

    private val logger          = play.api.Logger(this.getClass)
    private val fs              = FileSystems.getDefault
    private val keepAlive       = "KEEP-ALIVE"
    private val maxIdle         = 30.seconds
    private val maxLineSize     = 8192
    private val pollingInterval = 500.millis
    private val tailDir         = configuration.get[String]("custom.tailDir")
    private val txtExt          = "txt"

    private def tailPath(name:String, ext:String) = s"$tailDir/$name.$ext"

    /** Construct a WebSocket URL from the page request.
     *
     *  @param name the name path parameter from the request
     *  @return     the WebSocket URL for the client to use
     */
    private def wsUrl[A](name:String)(implicit request:Request[A]) : String = {
        val scheme = if (request.secure) "wss" else "ws"
        s"$scheme://${request.host}/ws/tail/${encodePathSegment(name, UTF_8)}"
    }

    /** The tail route.
     *
     *  @param name The path portion of the request URI
     *  @return     HTTP response
     */
    def tail(name:String) : Action[AnyContent] = Action { implicit request =>
        logger.debug(s"${request.method} tail(name = $name)")

        Ok(views.html.tail(name, wsUrl(name), keepAlive))
    }

    /** Open a WebSocket for tailing a file.
     *
     *  @param name content stream name
     *  @return     a WebSocket
     */
    def wsTail(name:String) : WebSocket = WebSocket.acceptOrResult[String, String] { request =>
        val pathString = tailPath(name, txtExt)
        logger.info(s"Opening web socket from ${request.remoteAddress} to $pathString.")

        Future.successful(Try {
            val path:Path = fs.getPath(pathString)
            if (!Files.exists(path)) {
                throw new FileNotFoundException
            }
            val lines:Source[String, NotUsed] = FileTailSource.lines(
                path            = path,
                maxLineSize     = maxLineSize,
                pollingInterval = pollingInterval
            )
            lines
        } match {
            case Failure(e:FileNotFoundException) => {
                logger.error(s"Source file $pathString not found: $e")
                Left(NotFound)
            }
            case Failure(e)                       => {
                logger.error(s"Error opening source file $pathString: $e")
                Left(InternalServerError)
            }
            case Success(lines)                   => {
                val in = Sink.foreach[String](s => logger.info(s"WebSocket msg from ${request.remoteAddress}: $s"))
                Right(Flow.fromSinkAndSource(in, lines).keepAlive(maxIdle, () => keepAlive))
            }
        })
    }
}
