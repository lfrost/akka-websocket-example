/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import scala.concurrent.Future

import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.mvc.Result
import play.api.test.{FakeRequest, Injecting}
import play.api.test.Helpers.{contentAsString, defaultAwaitTimeout}

/** Application controller tests
 */
class ApplicationControllerSpec extends PlaySpec with GuiceOneAppPerSuite with Injecting {
    "index route" should {
        "not display path if path is empty" in {
            val path = ""
            val controller = inject[controllers.ApplicationController]
            val result:Future[Result] = controller.index(path).apply(FakeRequest())
            val bodyText:String = contentAsString(result)
            bodyText must not include ("The path is ")
        }
        "display path if path is not empty" in {
            val path = "this/is/a/path"
            val controller = inject[controllers.ApplicationController]
            val result:Future[Result] = controller.index(path).apply(FakeRequest())
            val bodyText:String = contentAsString(result)
            bodyText must include (s"The path is $path.")
        }
    }
}
