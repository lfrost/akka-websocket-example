/*
 * Copyright (c) 2017-2020 Lyle Frost <lfrost@cnz.com>.
 */

import scala.concurrent.Future

import akka.stream.scaladsl.Flow
import org.scalatest.EitherValues
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.http.Status.NOT_FOUND
import play.api.http.websocket.Message
import play.api.mvc.Result
import play.api.test.{FakeRequest, Injecting}
import play.api.test.Helpers.{contentAsString, defaultAwaitTimeout, status}

/** Tail controller tests
 */
class TailControllerSpec extends PlaySpec with GuiceOneAppPerSuite with EitherValues with ScalaFutures with Injecting {
     val name = "ulysses"

    "tail route" should {
        "display title with file name" in {
            val controller = inject[controllers.TailController]
            val result:Future[Result] = controller.tail(name).apply(FakeRequest())
            val bodyText:String = contentAsString(result)
            bodyText must include (s"<h1>Tailing Text File $name</h1>")
        }
    }
    "tail WebSocket route" should {
        "succeed for file that exists" in {
            val controller = inject[controllers.TailController]
            val result:Future[Either[Result, Flow[Message, Message, _]]] = controller.wsTail(name).apply(FakeRequest())
            whenReady(result) { either =>
                either.isRight mustBe true
            }
        }
        "fail for file that does not exist" in {
            val name = "no-such-file"
            val controller = inject[controllers.TailController]
            val result:Future[Either[Result, Flow[Message, Message, _]]] = controller.wsTail(name).apply(FakeRequest())
            whenReady(result) { either =>
                either.isLeft mustBe true
                status(Future.successful(either.left.value)) mustBe NOT_FOUND
            }
        }
    }
}
