# akka-websocket-example

This project is for the article [WebSocket Connections with Akka Streams](https://www.pinnsg.com/websocket-connections-akka-streams/).  It is an example of using Scala, Play, Akka,and Alpakka for pushing content to a web client over a WebSocket.  It is forked from [scala-play-starter](https://gitlab.com/lfrost/scala-play-starter).  See the scala-play-starter README for details on building and running this application.

Beyond the general information in scala-play-starter, once the application is running, go to `http://localhost:9000/tail/ulysses`.  The contents of `data/ulysses.txt` should appear on the page.  To push more content to the page, append to the file with

```bash
echo 'Beyond the utmost bound of human thought.' >> data/ulysses.txt
```

Create other text files in the data directory (they must end in .txt), and then try the URL with the new name.


## Relevant Documentation

* [sbt documentation](https://www.scala-sbt.org/documentation.html)
* [Scala Standard Library API](https://www.scala-lang.org/api/2.13.8/)
* [Play manual](https://www.playframework.com/documentation/2.8.x/Home)
* [Play API](https://www.playframework.com/documentation/2.8.x/api/scala/index.html)
* [Akka Streams documentation](https://doc.akka.io/docs/akka/current/stream/)
* [Alpakka documentation](https://doc.akka.io/docs/alpakka/current/)
* [WebSocket API](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket)
